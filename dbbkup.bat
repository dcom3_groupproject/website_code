:: This script is for backing up the monogoDB
:: This script is to be run in the Task Scheduler

@echo off

:: move into the backups directory
CD C:\database_backups


:: Create a file name for the database output which contains the date and time.
set mydate=%date:~6,4%%date:~3,2%%date:~0,2%
set filename="database_%mydate%"


:: Lock the database and export the database
echo ---------- Running backup "%filename%" ----------
"C:\Program Files\MongoDB\Server\3.0\bin\mongo" admin --eval "printjson(db.fsyncLock())"
"C:\Program Files\MongoDB\Server\3.0\bin\mongodump" --out %filename%
"C:\Program Files\MongoDB\Server\3.0\bin\mongo" admin --eval "printjson(db.fsyncUnlock())"


:: ZIP the backup directory
echo ---------- Running backup "%filename%" ----------
"c:\apps\7za.exe" a -tzip "%filename%.zip" "%filename%"


:: Delete the backup directory
echo ---------- Deleting original backup directory "%filename%" ----------
rmdir "%filename%" /s /q

:: Store on f drive later maybe at the following
:: //ie.pilz/local/pilz/development/pas/backup


:: Delete files older than 35 days/ 5 weeks
forfiles -p "C:\database_backups" -s -m *. -d 35 -c "cmd /c del @path"

echo ---------- BACKUP COMPLETE ----------