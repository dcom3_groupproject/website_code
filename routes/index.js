var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var connection_string = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://localhost:27017/website';

router.get('/', function(req,res){
	res.sendFile(__dirname + '/login.html');
});

router.get('/newCustomer', function(req,res){
	res.sendFile(__dirname + '/addCustomer.html');
});

router.post('/addCustomerToDb', function(req,res)
{
	if(req.body.ID == "" || req.body.name == "" || req.body.address == "" || req.body.phone == "" || req.body.IP == "")
	{
		res.sendFile(__dirname + '/addCustomerFailed.html');
	}
	else
	{
		//Get a Mongo client to work with the Mongo server
		var MongoClient = mongodb.MongoClient;
		
		MongoClient.connect(connection_string, function(err,db)
		{	
			//Cant connect
			if(err)
			{
				console.log('unable to connect to the server', err);
			}
			//Connected to server
			else
			{
				console.log('Connection Established');
				//Get the documents collection
				var collection = db.collection('customers');
				collection.insert({"ID": req.body.ID, "name": req.body.name, "address": req.body.address, "phone": req.body.phone, "IP": req.body.IP, "status": "OK", "security": "OK", "fire": "OK", "carbon": "OK", "account": req.body.account }, function (err, result)
				{
					if (err) 
					{
						console.log(err);
					} 		
				});
				db.close();
			}
		});
		res.sendFile(__dirname + '/addCustomer.html');
	}	
});

router.post('/mainSearch', function(req, res)
{
	//Get a Mongo client to work with the Mongo server
	var MongoClient = mongodb.MongoClient;

	//Connect to the server
	MongoClient.connect(connection_string, function(err,db)
	{
		//Cant connect
		if(err)
		{
			console.log('unable to connect to the server', err);
		}
		//Connected to server
		else
		{	
			var search = '';
			search = req.body.search;
			var type = '';
			type = req.body.searchBy;
			console.log('Connection Established');
			//Get the documents collection
			var collection = db.collection('customers');
			if (type == 'Name')
			{
				collection.find({"account": "Full", "name":{$regex:search,$options:"$i"}}).toArray(function(err, result)
				{
					//Cant run sql
					if(err)
					{
						res.send(err);
					}
					//Pass the returned database documents to Jade
					else if(result.length)
					{
						res.render('main', {"main" : result});
					}
					//Returned is empty
					else
					{
						res.render('main', {"main" : result});
					}
					//Cloase database
					db.close();
				});
			}
			else
			{
				collection.find({"account": "Full", "ID":{$regex:search,$options:"$i"}}).toArray(function(err, result)
				{
					//Cant run sql
					if(err)
					{
						res.send(err);
					}
					//Pass the returned database documents to Jade
					else if(result.length)
					{
						res.render('main', {"main" : result});
					}
					//Returned is empty
					else
					{
						res.render('main', {"main" : result});
					}
					//Cloase database
					db.close();
				});
			}							
		}
	});
});

router.post('/mainFilter', function(req, res)
{
	//Get a Mongo client to work with the Mongo server
	var MongoClient = mongodb.MongoClient;
	
	//Connect to the server
	MongoClient.connect(connection_string, function(err,db)
	{
		//Cant connect
		if(err)
		{
			console.log('unable to connect to the server', err);
		}
		//Connected to server
		else
		{	
			var counties = '';
			counties = req.body.counties;
			var areas = '';
			areas = req.body.areas;
			console.log('Connection Established');
			//Get the documents collection
			var collection = db.collection('customers');
			if (counties == 'All')
			{
				collection.find({"account": "Full"}).toArray(function(err, result)
				{
					//Cant run sql
					if(err)
					{
						res.send(err);
					}
					//Pass the returned database documents to Jade
					else if(result.length)
					{
						res.render('main', {"main" : result});
					}
					//Returned is empty
					else
					{
						res.render('main', {"main" : result});
					}
					//Cloase database
					db.close();
				});
			}
			else if (areas == 'All')
			{
				collection.find({"account": "Full", "address":{$regex:counties,$options:"$i"}}).toArray(function(err, result)
				{
					//Cant run sql
					if(err)
					{
						res.send(err);
					}
					//Pass the returned database documents to Jade
					else if(result.length)
					{
						res.render('main', {"main" : result});
					}
					//Returned is empty
					else
					{
						res.render('main', {"main" : result});
					}
					//Cloase database
					db.close();
				});
			}
			else
			{
				collection.find({"account": "Full", "address":{$regex:areas,$options:"$i"}}).toArray(function(err, result)
				{
					//Cant run sql
					if(err)
					{
						res.send(err);
					}
					//Pass the returned database documents to Jade
					else if(result.length)
					{
						res.render('main', {"main" : result});
					}
					//Returned is empty
					else
					{
						res.render('main', {"main" : result});
					}
					//Cloase database
					db.close();
				});
			}							
		}
	});
});
	
router.get('/main', function(req, res)
{
	//Get a Mongo client to work with the Mongo server
	var MongoClient = mongodb.MongoClient;	
	//Connect to the server
	MongoClient.connect(connection_string, function(err,db)
	{
		//Cant connect
		if(err)
		{
			console.log('unable to connect to the server', err);
		}
		//Connected to server
		else
		{	
			console.log('Connection Established');
			//Get the documents collection
			var collection = db.collection('customers');
			collection.find({"account": "Full"}).toArray(function(err, result)
			{
				//Cant run sql
				if(err)
				{
					res.send(err);
				}
				//Pass the returned database documents to Jade
				else if(result.length)
				{
					res.render('main', {"main" : result});
				}
				//Returned is empty
				else
				{
					res.render('main', {"main" : result});
				}
				//Cloase database
				db.close();
			});
		}
	});
});

router.post('/checkLogin', function(req, res){
	//Get a Mongo client to work with the Mongo server
	var MongoClient = mongodb.MongoClient;
	//Define where the MongoDB server is
	
	MongoClient.connect(connection_string, function(err,db)
	{
		console.log (req.body.username);
		console.log (req.body.password);
		//Cant connect
		if(err)
		{
			console.log('unable to connect to the server', err);
		}
		//Connected to server
		else
		{	
			console.log('Connection Established');
			//Get the documents collection
			var collection = db.collection('users');
			collection.find({"username": req.body.username, "password": req.body.password}).toArray(function(err, result)
			{
				if (result.length)
				{
					res.redirect("/main");
				}
				else
				{
					collection.insert({"username": req.body.username, "password": req.body.password}, function (err, result){
                    if (err) 
                    {
                        console.log(err);
                    } 
                    });
                    db.close();
					res.sendFile(__dirname + '/loginFailed.html');
				}
				db.close();
			});
		}
	});
});

router.post('/mobileLogin', function(req, res) {
    //      127.12.79.2:27017
    //Get a Mongo client to work with the Mongo server
    var MongoClient = mongodb.MongoClient;
    //Define where the MongoDB server is
    
    MongoClient.connect(connection_string, function(err,db)
    {
        //Cant connect
        if(err)
        {
            throw err;
        }
        //Connected to server
        else
        {   
            var collection = db.collection('customers');
            var user = req.body.username;
            collection.find({"username": req.body.username, "password": req.body.password}).toArray(function(err, result)
            {
            	//Mobile user after logging into mobile application
                console.log(req.body.username);
                if (result.length)
                {
                    //Verified
                    var return_values = db.customers.find({}, {IP:1}).toArray(function(err, result){
                        res.json(return_values);
                    });
                    
                }
                else
                {
                   //Wrong Username or Password
                   //return boolean in json format
                   var success_login = {
                        success: "false"
                    }
                    collection.insert({"username": req.body.username, "password": req.body.password}, function (err, result){
                    if (err) 
                    {
                        console.log(err);
                    } 
                    });
                    db.close();
                    res.json(success_login);
                }
                db.close();
            });
        }
    });
});

router.post('/updateUser', function(req, res){
	var MongoClient = mongodb.MongoClient;
    //Define where the MongoDB server is
    MongoClient.connect(connection_string, function(err,db){
    	if(err){
    		throw err;
    	}else{
    		var collection = db.collection("customers");
    		collection.find({'username' : req.body.username});
    		collection.update({'account' : req.body.account}, {$set:{"account": req.body.account}});
    	}
    });
});

module.exports = router;
